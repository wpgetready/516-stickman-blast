﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Punto
{
    A,
    B
}
public class MoverEntrePuntos : MonoBehaviour {

    public Transform A;
    public Transform B;
    public float velocidad = 0.1f;
    public float dstMin = 0.5f;

    private Punto punto = Punto.A;
    private Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

	void Start () {

		if(Vector2.Distance(transform.position, A.position) >= Vector2.Distance(transform.position, B.position))
        {
            punto = Punto.A;
        }
        else
        {
            punto = Punto.B;
        }
	}
	
	
	void Update () {

        if(punto == Punto.A)
        {
            rb.MovePosition(Vector2.Lerp(transform.position, A.position, velocidad));
            if (Mathf.Abs(Vector2.Distance(transform.position, A.position)) <= dstMin)
            {
                punto = Punto.B;
            }
        }
        else if (punto == Punto.B)
        {
            rb.MovePosition(Vector2.Lerp(transform.position, B.position, velocidad));
            if (Mathf.Abs(Vector2.Distance(transform.position, B.position)) <= dstMin)
            {
                punto = Punto.A;
            }
        }
	}
}

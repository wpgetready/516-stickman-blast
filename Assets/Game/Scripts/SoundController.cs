﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

	public AudioSource fall;
	public AudioSource gameBg;
	public  AudioSource tap;
	public AudioSource button;
	public static SoundController instance;
    public SpriteRenderer spriteSound;
	public Sprite soundOff;
	public Sprite soundOn;

	void Start () {

		if (!instance) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} else {
			DestroyImmediate (gameObject);
		}
	}
	
	public void Button(){
		button.Play ();
	}

	public void  VolumeOn(){

        fall.mute = false;
        gameBg.mute = false;
        tap.mute = false;
        button.mute = false;

        spriteSound.sprite = soundOn;
	}
	public void volumeOff(){

        fall.mute = true;
        gameBg.mute = true;
        tap.mute = true;
        button.mute = true;

        spriteSound.sprite = soundOff;
    }
}

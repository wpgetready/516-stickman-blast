﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockedLevels : MonoBehaviour {
	
	public GameObject Locked;
	public GameObject Unlocked;

	void  Start (){
		UnLockLevels ();
	}

	private void  UnLockLevels (){
        if (PlayerPrefs.GetInt(gameObject.name) == 1)
        {
            Locked.SetActive(false);
            Unlocked.SetActive(true);
            GetComponent<Button>().interactable = true;

        }
        else
        {
            Locked.SetActive(true);
            Unlocked.SetActive(false);
            GetComponent<Button>().interactable = false;
        }
    }
	void Update()
    {
		if (Input.GetKeyDown (KeyCode.Delete)) {
			PlayerPrefs.DeleteAll ();
		}
    }
}


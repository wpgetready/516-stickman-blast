﻿using UnityEngine;
using UnityEngine.SceneManagement;


public enum UIStates
{
    noState, gameCompleted, levelFailed, sceneRestart
}

public class InGameUI : MonoBehaviour {
    private const string URL_NEXT_APP = "https://play.google.com/store/apps/details?id=com.alexandriabookseng.book001";

    /**START EVENT ADAPTER**/
    private UIStates myUIState = UIStates.noState;
    public static event System.Action<UIStates> UIStateChanged = delegate { };

    public UIStates UIState
    {
        get
        {
            return myUIState;
        }
        private set
        {
            if (value != myUIState)
            {
                myUIState = value;
                UIStateChanged(myUIState);
            }
        }
    }
    /**END EVENT ADAPTER**/

    public GameObject All;
	public GameObject LevelFinish;
	SoundController mySound;
	public GameObject pause;

    /*
    static int nivelesGanados = 0;
    float timeStartLvl = 0f;            //Aca guardamos en segundos el tiempo que lleva se lleva jugando cuando se inicia el nivel
    static float tiempoJugando = 0;     //Aca guardamos el tiempo que se lleva jugando todo el juego (cada vez que se gana un nivel) para saber cuando pasaron 5 minutos
    */

	public bool GameComplete;

	void Start () {
		Time.timeScale = 1;
		All.SetActive (true);
		LevelFinish.SetActive (false);
		mySound = FindObjectOfType<SoundController>();
		PlayerPrefs.SetInt (SceneManager.GetActiveScene().name,1); //set the game enabled to re enter, not at the finish, it's a torture!
    }

    void OnDestroy() {}

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag.Contains("Over"))
		{
            UIState = UIStates.gameCompleted;
            GameComplete = true;

            if (mySound != null)
            {
                mySound.fall.Play();
            }

            //Special case: check if we are in the last screen
            if (SceneManager.GetActiveScene().buildIndex == 51)
            {
             //   Application.OpenURL(URL_NEXT_APP); //we need to have a link to the next game! But we don't have it...yet.

                SceneManager.LoadScene("MainMenuUI");

            }
            else
            {
                LevelFinish.SetActive(true);
                All.SetActive(false);
                pause.SetActive(false);
            }
		}
	}

	public  void PauseButtonScript(){
		Time.timeScale = 0;
	}
	public void Home(){
		SceneManager.LoadScene ("MainMenuUI");
		if (mySound !=null)   {
			mySound.fall.Play();
			mySound.button.Play();	
		}
    }
	public void GameFinishScript(){
		Time.timeScale = 0;
	}
	public void LevelFailedScript(){
        UIState = UIStates.levelFailed;
        Time.timeScale = 0;
	}

	public void Restart(){
        UIState = UIStates.sceneRestart;
        SceneManager.LoadScene (SceneManager.GetActiveScene().name);
		if (mySound != null){
			mySound.button.Play ();	
		}
		Time.timeScale= 1;
	}
	public void Resume(){
		Time.timeScale = 1;
	}
	public void NextLevel(){
	
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
		Time.timeScale = 1;
		if (mySound !=null)
		{
			mySound.button.Play ();	
		}
	}
	public void PreviousLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex - 1);
		Time.timeScale = 1;

	}
	public void Help(){
		Time.timeScale = 0;
	}
	public void Help_Back(){
		//Help_desk.SetActive (false);
		Time.timeScale = 1;
	}

	public void LoadLevels(){
		if (mySound != null){
			mySound.button.Play ();	
		}
		PlayerPrefs.SetInt ("main", 1);
		SceneManager.LoadScene ("MainMenuUI");
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour {

    public GameObject currentPlayer;
    public GameObject prefabPlayer;
    public Transform posPlayer;
    public string tagPlayer = "Over";
    public GameObject[] objetos;

    private Vector2[] posiciones;
    private Rigidbody2D rbPlayer;

	//20180325: FZSM: Por favor documentar...

    void Start()
    {
		//Recordar las posiciones de cada objeto que hay que volver a setear.
		//Estos son todos los objetos 'explotables': globos y ladrillos rojos.

        posiciones = new Vector2[objetos.Length];

        for (int i = 0; i < objetos.Length; i++)
        {
            posiciones[i] = objetos[i].transform.position;
        }
    }

    public void ResetiarPosiciones()
    {
        /*
        for (int i = 0; i < objetos.Length; i++)
        {
            if(objetos[i].name == namePlayer)
            {
                DetenerRigidbody2D(objetos[i]);

                //Revisamos si tiene hijos y si estos tambien tienen, resetiamos todos los Rigidbody2D
                for (int j = 0; j < objetos[i].transform.childCount; j++)
                {
                    DetenerRigidbody2D(objetos[i].transform.GetChild(j).gameObject);

                    for (int k = 0; k < objetos[i].transform.GetChild(j).childCount; k++)
                    {
                        DetenerRigidbody2D(objetos[i]
                            .transform.GetChild(j)
                            .GetChild(k).gameObject);
                    }
                }
            }
            */


        currentPlayer.SetActive(false); //Desactivar jugador
        currentPlayer = Instantiate(prefabPlayer, posPlayer.position, Quaternion.identity); //reinstanciarlo en el lugar de inicio
        rbPlayer = currentPlayer.GetComponent<Rigidbody2D>(); 
		currentPlayer.transform.GetChild(0).tag = tagPlayer; //El tag que se pasa no es el del player sino un nivel más arriba, por lo que es preciso ubicar el player y tagearlo.
															 //No me he fijado en profundidad, porque hay que RE tagearlo?
		//currentPlayer.tag = tagPlayer;
		//Recorrer todos los objetos a reactivar
        for (int i = 0; i < objetos.Length; i++)
        {
            DetenerRigidbody2D(objetos[i]); //Detenerlos si están en movimiento
            objetos[i].SetActive(true); //Desplegarlos.
            objetos[i].transform.position = posiciones[i]; //Reiniciar posiciones
            //Unir los objetos al player. Esta sentencia solo funciona para esta pantalla donde todos los objetos se unen al player
			//PERO es un caso excepcional. Esta rutina no es escalable(!)
			objetos[i].GetComponent<HingeJoint2D>().connectedBody = rbPlayer; 
            DistanceJoint2D dj2D = objetos[i].GetComponent<DistanceJoint2D>();
			//En caso de que tengan un DistanceJoint, conectarlo
            if(dj2D != null)
            {
                dj2D.connectedBody = rbPlayer;
            }
			//Conectar la linea del objeto al player.
            objetos[i].GetComponent<Linescript>().ConnectedTransform(rbPlayer.transform);
            
        }        
    }

    private void DetenerRigidbody2D(GameObject obj)
    {
        Rigidbody2D rb2D = obj.GetComponent<Rigidbody2D>();
        if(rb2D != null)
        {
            rb2D.velocity = Vector2.zero;
            rb2D.angularVelocity = 0f;
        }
    }
}

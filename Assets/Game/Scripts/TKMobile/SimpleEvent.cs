﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleEvent : MonoBehaviour {

    public string objName = "red";
    public GameObject particles;
    SoundController mySound;

    void Start()
    {
        mySound = FindObjectOfType<SoundController>();
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {

            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider == null)
            {
                return;
            }
            if (hit.collider.name == objName)
            {
                Evento evento = hit.collider.GetComponent<Evento>();
                if (evento != null) evento.Activar();
                if (mySound != null) mySound.tap.Play();
                Instantiate(particles, hit.transform.position, Quaternion.identity);
            }
        }
    }
}

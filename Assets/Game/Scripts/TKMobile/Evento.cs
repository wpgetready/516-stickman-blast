﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Evento : MonoBehaviour {

    public UnityEvent OnClick;
    public float timeNextEvent = 1.0f;
    public UnityEvent PostOnClick;

    public void Activar()
    {
        OnClick.Invoke();
        Invoke("SegundoEvento", timeNextEvent);
    }
    private void SegundoEvento()
    {
        PostOnClick.Invoke();
    }
}

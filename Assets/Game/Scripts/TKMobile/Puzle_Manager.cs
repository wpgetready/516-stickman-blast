﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Puzle_Manager : MonoBehaviour {

    public static string clave = ""; //Aca se va a escribir la clave
    [SerializeField] private int claveObjetivo = 0; //Aca ponemos desde el inspector la clave que tiene que adivinar el jugador
    private static string claveCorrecta = "";   //Esta variable la usa la clase para guardar "claveObjetivo" como un string
    public UnityEvent EventClaveDescubierta;    //Metodos que se ejecutan al descubrir la clave

    private bool puzleCompletado = false;

    void Start()
    {
        clave = "";     //Al ser variables estaticas, no se reinicia al cambiar de nivel asi que la reiniciamos al instanciar el objeto
        claveCorrecta = string.Format("{0}", claveObjetivo);     //Transformamos la clave de tipo int en string
    }
    
    void Update()
    {
        if (puzleCompletado) return;

        if (clave == claveCorrecta)
        {
            EventClaveDescubierta.Invoke();
            puzleCompletado = true;
        }
    }
}

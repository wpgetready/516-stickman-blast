﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection.Emit;

/// <summary>
/// This class is almost identical to Hide_Rope.cs except it allows similar objectGame names
/// </summary>
public class Explode_object : MonoBehaviour {
	[Tooltip("Object name or similar. For example if name is Red the following names will be accepted: Red_01, Red(02) etc.")]
		public string objName;
	[Tooltip("Particles object/prefab")]
		public GameObject particles;
		SoundController mySound;
		void Start(){
			mySound = FindObjectOfType<SoundController> ();
		}

		void Update(){

			if (Input.GetMouseButtonDown (0)) {

				Vector2 worldPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				RaycastHit2D hit = Physics2D.Raycast (worldPoint, Vector2.zero);
				if (hit.collider == null) {
					return;
				}
			if (hit.collider.name.Contains (objName)) {
					//particles.SetActive (true);
					hit.collider.gameObject.SetActive (false);
					Instantiate (particles, hit.transform.position,Quaternion.identity);
					if (mySound != null)
					{
						mySound.tap.Play();
					}

				}
			}
		}
}

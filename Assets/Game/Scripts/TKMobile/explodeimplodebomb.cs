﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this class is an extension of explodebomb, handling the black hole event.
/// When the bomb power is too much, a black hole is made instead a repulsion one.
/// This class handles this scenario
/// </summary>
public class explodeimplodebomb : MonoBehaviour {

	[Tooltip("This object handles the explosion effect")]
	public GameObject explosionParticles;
	[Tooltip("This is the effector when artifact explodes or implodes.")]
	public GameObject explosionEffector;
	[Tooltip("This is the effector when a black hole is made")]
	public GameObject implosionEffector;
	[Tooltip("blackHolePicture")]
	public Sprite blackHole;

	//Notes about initialization
	//A bomb explodes, generating a force repulsion and a particle animation
	//I based the effects on some initial values. 
	//Since both are using same explosionPower, to balance both effect there is a third parameter
	//to control proper particle size
	//This is ONE solution of several. We could adjust potentiometer values, use two variables to control, etc. 
	//Since this is an invention, it's up to me.
	[Tooltip("Explosion particle effect size")]
	public float initialParticleSize = 4.00f; //Initial particle size. Used for reference to calculate exact size depending on explosion Power. Don't change it...
	[Tooltip("Explosion particle softener, to make a balance between power and particle size. (values between 0.0 (particle size never changes) - 10.0(gigantesic))")]
	public float particleMultiplier = 1;
	[Tooltip("Repulsion force for bomb")]
	public float initialForceMagnitude = 30.00f ; //Initial force magnitude for bomb

	//public float forceVariation =10.00;
	[Tooltip("How much time before object is destroyed after explosion,in seconds")]
	private float lifeTime=1.0f; //after this value, the object is erased completely(in seconds)
	//Wait few seconds before destroy object completely.
	[Tooltip("This is a measure of bomb power which it will be controlled by potenciometer")]
	public float explosionPower=0;
	[Tooltip("Minimal power to make a black hole")]
	public float minPowerBlackHole = 25.0f; //min power required for making a black hole. This currently represents half of potentiometer.
	[Tooltip("Time in seconds for appearing black hole")]
	private  float waitforBH = 0.75f;

	private bool activateDestroy = false;
	private SpriteRenderer pictureBomb;
	private ParticleSystem particleS;
	private PointEffector2D explosionE;
	private PointEffector2D implosionE;

	void Start () {
		pictureBomb = gameObject.GetComponent<SpriteRenderer> ();
		particleS = explosionParticles.GetComponent<ParticleSystem> (); //We need this to control effect using potenciometer
		explosionE = explosionEffector.GetComponent<PointEffector2D> (); //We need this to control effect using potenciometer
		implosionE = implosionEffector.GetComponent<PointEffector2D> (); 
	}

	/// <summary>
	/// The explode process 
	/// </summary>
	public void explode() {
		pictureBomb.enabled=false; //Hide the bomb picture
		explosionParticles.SetActive (true); //activate particles, making the explosion effect
		//Now we need to determine how it's going to be:
		//If we selected not too much power, a normal bomb is placed
		//Otherwise a black hole is created.
		if (explosionPower>minPowerBlackHole) {
			StartCoroutine ("BlackHole");
		} else { //fuse a normal bomb
			explosionEffector.SetActive (true); //activate effector, making the repulsion effect.
			activateDestroy = true;  //Program for deactivation after dissapear
		}
	}

	private IEnumerator BlackHole() {

		gameObject.GetComponent<CircleCollider2D> ().enabled = false; //deactivate the collider, otherwise objects can't be attracted to the center.
		yield return new WaitForSeconds (waitforBH);
		pictureBomb.sprite = blackHole; 
		pictureBomb.enabled = true;

		implosionEffector.SetActive (true);
	}

	// Update is called once per frame
	void Update () {
		if (activateDestroy) {
			lifeTime = lifeTime - Time.deltaTime; //Time.deltaTime is the time it take for every frame.
			if (lifeTime<=0f) {
				Destroy (explosionParticles);
				Destroy (explosionEffector);
				Destroy (implosionEffector);
				Destroy (gameObject);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		//Calculate explosion size depending on explosion power.
		var ma = particleS.main;
		float expPower =  initialParticleSize + explosionPower * particleMultiplier;

		ma.startSizeX =expPower ;//Important: potenciometer needs to be properly regulated (instead 0..1 maybe 0..10?)
		ma.startSizeY = expPower ;//Explosion power will be handled by a potentiometer.
		explosionE.forceMagnitude = initialForceMagnitude + explosionPower;

		//anything that touches it will trigger the explosion
		explode ();
	}


}

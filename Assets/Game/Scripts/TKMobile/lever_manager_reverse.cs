﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lever_manager_reverse : MonoBehaviour {

	public GameObject[] go_switch;
	public GameObject TrapPlatform;
	[Tooltip ("If there are more than one switches, indicates how switches operates each other (AND,OR,XOR)")]
	public string SwitchMode = "OR";


	private switch_reverse _sc;
	private HingeJoint2D _hj;

	// Use this for initialization
	void Start () {
		//This is an first attempt of using switch contoller
		//Get objects to check conditions

		_hj = TrapPlatform.GetComponent<HingeJoint2D> ();
	}

	// Update is called once per frame

	void Update () {
		/* If I use the code in this way I will activate but I won't deactivate, which it would be usefule in some cases.
		if (_sc.Is_On==true) {
			Debug.Log ("opening gates...");
			_hj.useMotor = true;
		} 
*/
		if (go_switch.Length == 0) {
			Debug.Log("No elements");
			return;
		}
		if (go_switch.Length==1)
		{
			_sc = go_switch[0].GetComponent<switch_reverse>();
			_hj.useMotor = _sc.Is_On;
			return;
		}
		bool flag = false;
		switch (SwitchMode)
		{
		case "AND":
			flag = true;
			break;
		case "XOR":
			flag = false;
			break;
		default:
			flag = false;
			break;
		}
		foreach (GameObject item in go_switch)
		{
			_sc = item.GetComponent<switch_reverse>();
			switch (SwitchMode)
			{
			case "AND":
				flag =flag && _sc.Is_On;
				break;
			case "XOR":
				flag = flag ^ _sc.Is_On;
				break;
			default:
				flag = flag || _sc.Is_On;
				break;
			}
		}
		_hj.useMotor = flag;
		Debug.Log ("flag:" + flag);
}
}


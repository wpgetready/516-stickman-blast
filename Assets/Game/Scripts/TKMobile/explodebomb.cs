﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explodebomb : MonoBehaviour {

	[Tooltip("This object handles the explosion effect")]
	public GameObject explosionParticles;
	[Tooltip("This is the effector an object which affect surrounding when artifact explodes or implodes.")]
	public GameObject explosionEffector;
	[Tooltip("This is a measure of bomb power which it will be controlled by potenciometer")]
	public float explosionPower=0;
    /*
	Notes about initialization
	A bomb explodes, generating a force repulsion and a particle animation
	I based the effects on some initial values. 
	Since both are using same explosionPower, to balance both effect there is a third parameter
	to control proper particle size
	This is ONE solution of several. We could adjust potentiometer values, use two variables to control, etc. 
	Since this is an invention, it's up to me.
    */
	[Tooltip("Explosion particle effect size")]
	public float initialParticleSize = 4.00f; //Initial particle size. Used for reference to calculate exact size depending on explosion Power. Don't change it...
	[Tooltip("Explosion particle softener, to make a balance between power and particle size. (values between 0.0 (particle size never changes) - 10.0(gigantesic))")]
	public float particleMultiplier = 1;
	[Tooltip("Repulsion force for bomb")]
	public float initialForceMagnitude = 30.00f ; //Initial force magnitude for bomb

	//public float forceVariation =10.00;
	[Tooltip("How much time before object is destroyed after explosion")]
	public float lifeTime; //after this value, the object is erased completely.
	//Wait few seconds before destroy object completely.
	private bool activateDestroy = false;
	private SpriteRenderer pictureBomb;
	private ParticleSystem particleS;
	private PointEffector2D pointE;

	

	void Awake () {
		pictureBomb = gameObject.GetComponent<SpriteRenderer> ();
		particleS = explosionParticles.GetComponent<ParticleSystem> (); //We need this to control effect using potenciometer
		pointE = explosionEffector.GetComponent<PointEffector2D> (); //We need this to control effect using potenciometer
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        //Calculate explosion size depending on explosion power.
        var ma = particleS.main;
        float expPower = initialParticleSize + explosionPower * particleMultiplier;

        ma.startSizeX = expPower;//Important: potenciometer needs to be properly regulated (instead 0..1 maybe 0..10?)
        ma.startSizeY = expPower;//Explosion power will be handled by a potentiometer.
        pointE.forceMagnitude = initialForceMagnitude + explosionPower;

        //anything that touches it will trigger the explosion
        explode();
    }

    public void explode()
    {
        pictureBomb.enabled = false; //Hide the bomb picture
        explosionParticles.SetActive(true); //activate particles, making the explosion effect
        explosionEffector.SetActive(true); //activate effector, making the repulsion effect.
        activateDestroy = true;
    }

    // Update is called once per frame
    void Update () {
		if (activateDestroy) {
			lifeTime = lifeTime - Time.deltaTime; //Time.deltaTime is the time it take for every frame.
			if (lifeTime<=0f) {
				Destroy (explosionParticles);
				Destroy (explosionEffector);
				Destroy (gameObject);
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switch_reverse : MonoBehaviour {

		public bool Is_On = false; //by default, however this might change.

		public Sprite SwitchOn;
		public Sprite SwitchOff;

		private float speed;
		private bool flag;

		[Tooltip("How many frames waiting before switching state")]
		public int waitFrames = 25;
		[Tooltip("Object which has a Hinge Joint 2D")]
		public GameObject MotorizedObject;
		private Rotation rs;
		private SpriteRenderer theSpriteRenderer;

		private int counter = 0;
		// Use this for initialization
		void Start () {
		theSpriteRenderer = GetComponent<SpriteRenderer>();
			rs = MotorizedObject.GetComponent<Rotation> ();
			flag = false;
			switchSprite();
		}

		void switchSprite()
		{
			theSpriteRenderer.sprite = (Is_On) ? SwitchOn : SwitchOff;
		}
		
		void switchSpeed()  		
		{
		rs.speed = -rs.speed;
		}

		void FixedUpdate()
		{
			if (counter == 0) return;
			counter--;
		}

		/// <summary>
		/// Triggered everytime the switch is touched
		/// the counter trick is for avoiding to call too many times in a fraction of time.
		/// </summary>
		/// <param name="coll"></param>
		void OnCollisionEnter2D(Collision2D other)
		{
			if (counter == 0)
			{
				Debug.Log("Colision Enter");
				counter = waitFrames;
				Is_On = !Is_On; //Reverse the flag
				switchSprite();
				switchSpeed ();
				
			}
		}
	}

	



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to control levers
/// </summary>
public class hide_object : MonoBehaviour {

    public GameObject[] go_switch;
	public GameObject objectToHide;
	public bool InvertOperation=false;
    [Tooltip ("If there are more than one switches, indicates how switches operates each other (AND,OR,XOR)")]
    public string SwitchMode = "OR";

     
    private switch_controller _sc;
	//private HingeJoint2D _hj;

	// Use this for initialization
	void Awake () {
		//This is an first attempt of using switch contoller
		//Get objects to check conditions
		
//		_hj = objectToHide.GetComponent<HingeJoint2D> ();

	}
	
	// Update is called once per frame

	void Update () {
        /* If I use the code in this way I will activate but I won't deactivate, which it would be usefule in some cases.
		if (_sc.Is_On==true) {
			Debug.Log ("opening gates...");
			_hj.useMotor = true;
		} 
*/
        if (go_switch.Length == 0) {
            Debug.Log("No elements");
            return;
        }
        if (go_switch.Length==1)
        {
            _sc = go_switch[0].GetComponent<switch_controller>();

			if (!InvertOperation){
				objectToHide.SetActive (_sc.Is_On);	
			}
			else
			{
				objectToHide.SetActive (!_sc.Is_On);	
			}

            //_hj.useMotor = _sc.Is_On;
            return;
        }
        bool flag = false;
        switch (SwitchMode)
        {
            case "AND":
                flag = true;
                break;
            case "XOR":
                flag = false;
                break;
            default:
                flag = false;
                break;
        }
        foreach (GameObject item in go_switch)
        {
            _sc = item.GetComponent<switch_controller>();
            switch (SwitchMode)
            {
                case "AND":
                    flag =flag && _sc.Is_On;
                    break;
                case "XOR":
                    flag = flag ^ _sc.Is_On;
                    break;
                default:
                    flag = flag || _sc.Is_On;
                    break;
            }
        }
        //_hj.useMotor = flag;
		if (!InvertOperation){
			objectToHide.SetActive (flag);	
		}
		else
		{
			objectToHide.SetActive (!flag);	
		}

		Debug.Log ("flag:" + flag);

    }
}

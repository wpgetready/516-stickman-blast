﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blackHoleDestroy : MonoBehaviour {

	/// <summary>
	/// This is activated when an object enters into the black hole.
	/// We can't simply destroy the objects. Since we need to wait a little o apply some effect, we need to collect all the objects
	/// and delete it one by one.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerEnter2D(Collider2D other) {
		StartCoroutine ("destroyObject", other.gameObject);
	}

	IEnumerator destroyObject(GameObject toDestroy){
		yield return new WaitForSeconds (1.0f);
		Destroy (toDestroy);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to control levers
/// </summary>
public class reverse_manager : MonoBehaviour {

    public GameObject[] go_switch;
	public GameObject movingOrRotatingObject;
    [Tooltip ("If there are more than one switches, indicates how switches operates each other (AND,OR,XOR)")]
    public string SwitchMode = "OR";

     
    private switch_controller _sc;
	private HingeJoint2D _hj;

	// Use this for initialization
	void Start () {
		//This is an first attempt of using switch contoller
		//Get objects to check conditions
		
		_hj = movingOrRotatingObject.GetComponent<HingeJoint2D> ();
	}
	
	// Update is called once per frame

	void Update () {
        /* If I use the code in this way I will activate but I won't deactivate, which it would be usefule in some cases.
		if (_sc.Is_On==true) {
			Debug.Log ("opening gates...");
			_hj.useMotor = true;
		} 
*/
        if (go_switch.Length == 0) {
            Debug.Log("No elements");
            return;
        }
        if (go_switch.Length==1)
        {
			var m2 = _hj.motor;
			m2.motorSpeed = -m2.motorSpeed;
			Debug.Log ("m2.motorSpeed=" + m2.motorSpeed);
            return;
        }
        bool flag = false;
        switch (SwitchMode)
        {
            case "AND":
                flag = true;
                break;
            case "XOR":
                flag = false;
                break;
            default:
                flag = false;
                break;
        }
        foreach (GameObject item in go_switch)
        {
            _sc = item.GetComponent<switch_controller>();
            switch (SwitchMode)
            {
                case "AND":
                    flag =flag && _sc.Is_On;
                    break;
                case "XOR":
                    flag = flag ^ _sc.Is_On;
                    break;
                default:
                    flag = flag || _sc.Is_On;
                    break;
            }
        }
		var m = _hj.motor;
		m.motorSpeed = -m.motorSpeed;
		Debug.Log ("motor speed:" + m.motorSpeed);

    }
}

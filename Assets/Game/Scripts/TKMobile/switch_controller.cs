﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Yeah, you will learn very fast that making a C# class named switch is a bad idea....since switch is a reserved word
/// It works, however the event is called many times. How to fix this?
/// Solution 
/// </summary>
public class switch_controller : MonoBehaviour
{

    public bool Is_On = false; //by default, however this might change.

    public Sprite SwitchOn;
    public Sprite SwitchOff;
    private SpriteRenderer theSpriteRenderer;
	[Tooltip("How many frames waiting before switching state")]
    public int waitFrames = 25;

    private int counter = 0;
    // Use this for initialization
    void Start () {
        
        theSpriteRenderer = GetComponent<SpriteRenderer>();
        switchSprite();
    }

    void switchSprite()
    {
        if (Is_On)
        {
            theSpriteRenderer.sprite = SwitchOn;
        }
        else
        {
            theSpriteRenderer.sprite = SwitchOff;
        }
       
    }

    // Update is called once per frame
    void Update () {
		
	}

    void FixedUpdate()
    {
        if (counter == 0) return;
            counter--;
        
    }

    void OnCollisionExit2D(Collision2D other)
    {
        Debug.Log("Colision Exit");
    }

    /// <summary>
    /// Triggered everytime the switch is touched
    /// the counter trick is for avoiding to call too many times in a fraction of time.
    /// </summary>
    /// <param name="coll"></param>
    void OnCollisionEnter2D(Collision2D other)
    {
        if (counter == 0)
        {
            Debug.Log("Colision Enter");
            counter = waitFrames;
			Is_On = !Is_On; //Reverse the flag
			switchSprite();
        }
    }
}

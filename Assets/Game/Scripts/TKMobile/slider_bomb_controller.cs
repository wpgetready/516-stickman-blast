﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class slider_bomb_controller : MonoBehaviour {

	public GameObject[] bombs;
	private Slider sl;
	// Use this for initialization
	void Start () {
		sl = gameObject.GetComponent<Slider> ();
	}
	
	// Update is called once per frame
	void Update () {
		foreach (GameObject b in bombs) {
			if (b!=null){ //we could be trying to access an object we already exploded... so we need to check if exist first.
				b.GetComponent<explodeimplodebomb> ().explosionPower = sl.value;	
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to control the object gravity based on switches.
/// </summary>
public class gravity_manager : MonoBehaviour {

	private Rigidbody2D _rb;

	void Start() {
		_rb = gameObject.GetComponent<Rigidbody2D> ();
	
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.GetComponent<switch_controller> () != null) {
			_rb.gravityScale = -_rb.gravityScale;
		}
	}



    
}

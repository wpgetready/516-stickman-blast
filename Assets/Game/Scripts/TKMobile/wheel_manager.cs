﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to control levers
/// </summary>
public class wheel_manager : MonoBehaviour {

    public GameObject[] go_switch;
	//public GameObject[] Wheels;
	public GameObject WheelController;
    [Tooltip ("If there are more than one switches, indicates how switches operates each other (AND,OR,XOR)")]
    public string SwitchMode = "OR";

     
    private switch_controller _sc;
	//private WheelJoint2D _wh;

	// Use this for initialization
	void Start () {
		//This is an first attempt of using switch contoller
		//Get objects to check conditions
		
		//_wh = TrapPlatform.GetComponent<WheelJoint2D> ();
	}
	
	// Update is called once per frame

	void Update () {
        /* If I use the code in this way I will activate but I won't deactivate, which it would be usefule in some cases.
		if (_sc.Is_On==true) {
			Debug.Log ("opening gates...");
			_hj.useMotor = true;
		} 
*/
        if (go_switch.Length == 0) {
            Debug.Log("No elements");
            return;
        }
		/*
        if (go_switch.Length==1)
        {
            _sc = go_switch[0].GetComponent<switch_controller>();
			Wheels[0].GetComponent<WheelJoint2D>().useMotor = _sc.Is_On;
            return;
        }*/
        bool flag = false;
        switch (SwitchMode)
        {
            case "AND":
                flag = true;
                break;
            case "XOR":
                flag = false;
                break;
            default:
                flag = false;
                break;
        }
        foreach (GameObject item in go_switch)
        {
            _sc = item.GetComponent<switch_controller>();
            switch (SwitchMode)
            {
                case "AND":
                    flag =flag && _sc.Is_On;
                    break;
                case "XOR":
                    flag = flag ^ _sc.Is_On;
                    break;
                default:
                    flag = flag || _sc.Is_On;
                    break;
            }
        }

		WheelJoint2D[] wheels = WheelController.GetComponents<WheelJoint2D> ();

		foreach (var w in wheels) {
			w.useMotor = flag;	
		}
        
//		Debug.Log ("flag:" + flag);

    }
}

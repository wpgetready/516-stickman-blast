﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DetectCollider : MonoBehaviour {

    [SerializeField] private string tagTarget;
    [SerializeField] private UnityEvent OnCollisionEnter;
    [SerializeField] private UnityEvent OnTriggerEnter;

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag(tagTarget))
        {
            OnCollisionEnter.Invoke();
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(tagTarget))
        {
            OnTriggerEnter.Invoke();
        }
    }
    public void SetScale(float scale)
    {
        transform.localScale = new Vector2(scale, scale);
    }
}

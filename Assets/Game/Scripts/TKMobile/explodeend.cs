﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explodeend : MonoBehaviour {

	public float lifeTime;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		lifeTime = lifeTime - Time.deltaTime; //Time.deltaTime is the time it take for every frame.
		if (lifeTime<=0f) {
			Destroy (gameObject);
		}
		//Debug.Log (Time.unscaledDeltaTime);
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// DO NOT USE THIS SCRIPT!!!!!!!
/// Line Renderer is extremely limited.
/// This script intention is to hang many objects from one point, but LineRendere fails.
/// We cannot use a Single LineRenderer for drawing multiples lines. It's just not posible.
/// </summary>
public class MultipleLineScript : MonoBehaviour {

	public Transform[] ConnectedObj;
	LineRenderer linerend;

	List<LineRenderer> lines = new List<LineRenderer>();
	// Use this for initialization
	void Start () {
		linerend = GetComponent<LineRenderer> ();
		lines.Clear ();
		foreach (var line in ConnectedObj) {
			lines.Add (linerend);
		}
	}

	// Update is called once per frame
	void Update () {
		int counter = 0;
		foreach (Transform item in ConnectedObj) {
			lines[counter].SetPosition (0,transform.position);
			lines[counter].SetPosition (1, item.position);	
			counter++;
		}
	}
}

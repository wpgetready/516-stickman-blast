﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Puzle : MonoBehaviour {

    [SerializeField] private int valor = 0;

    void OnDestroy()
    {
        Puzle_Manager.clave = string.Format("{0}{1}", Puzle_Manager.clave, valor);
    }
    void OnDisable()
    {
        Puzle_Manager.clave = string.Format("{0}{1}", Puzle_Manager.clave, valor);
    }
}

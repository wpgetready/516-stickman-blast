﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Documentation in the foot of the page
public class DelegateManager : MonoBehaviour {

    public static DelegateManager instance;
    AdmobStates currAdState = AdmobStates.noState;
    UIStates currUIState = UIStates.noState;

   public int showAdsEveryNLevels = 2;
    public int showAdsAfterRetry = 3;
    bool flagInterstitial = false;

    private int showAdsEveryNLevelsCounter;
    private int showAdsAfterRetryCounter;

    // Use this for initialization
    void Start () {
        if (!instance)
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }

        showAdsEveryNLevelsCounter = showAdsEveryNLevels;
        showAdsAfterRetryCounter = showAdsAfterRetry;
    }
	
	void Update () {}

    void OnEnable()     //Suscribe to the events, of every object needed
    {
        InGameUI.UIStateChanged += InGameUI_UIStateChanged;
        AdmobManager.AdmobStateChanged += AdmobManager_AdmobStateChanged;
    }

    void OnDisable() //Unsuscribe to the events when disabled
    {
        InGameUI.UIStateChanged -= InGameUI_UIStateChanged;
        AdmobManager.AdmobStateChanged -= AdmobManager_AdmobStateChanged;
    }

    private void AdmobManager_AdmobStateChanged(AdmobStates obj)
    {
        currAdState = obj;
        //Filter here unneeded events
        Debug.Log("FZ:DelegateManager AdmobManager Fired event:" + obj.ToString());
        switch (obj)
        {
            case AdmobStates.noState:
                break;
            case AdmobStates.interClosed:
                //Interstitial is closed, it is a good time to request another one.
                if (flagInterstitial)
                {
                    AdmobManager.instance.RequestInterstitialNormal(); //Normal Interstitial
                    Debug.Log("FZ: DM asking form Normal Interstitial");
                } else
                {
                    AdmobManager.instance.RequestInterstitialVideo(); //Video Interstitial
                    Debug.Log("FZ: DM asking for Video Interstitial");
                }
                flagInterstitial = !flagInterstitial;
                break;
            case AdmobStates.videoRewarded:
                break;
            case AdmobStates.videoClosed:
                break;
            default:
                break;
        }
    }

    private void InGameUI_UIStateChanged(UIStates obj)
    {
        //Rules:
        //Display an interstitial every n levels (currently 2)
        //Display an interstitial every m retries (currently 3)
        //Additional rule: if an interstitial is displayed, reset counters, to avoid displaying too much ads.
        //TODO:
        //Additional rule 2: see if posible to alternate between interstitial fixed and interstitial video.
        //for that we need to change the admob code.
        currUIState = obj;
        Debug.Log("FZ:DelegateManager InGameUI_UIStateChanged Fired event:" + obj.ToString());
        switch (obj)
        {
            case UIStates.noState:
                break;
            case UIStates.gameCompleted:
                showAdsEveryNLevels--; //A level is completed

                if (showAdsEveryNLevels==0)
                {
                    fireInterstitial(); //fire ad, reset counters
                } else
                {
                    Debug.Log("FZ:Interstitial skipped , is not the time for display");
                }
                showAdsAfterRetryCounter = showAdsAfterRetry;
                break;
            case UIStates.sceneRestart:
                Debug.Log("FZ:Level Restarted.");
                showAdsAfterRetryCounter--;
                if(showAdsAfterRetryCounter ==0)
                {
                    fireInterstitial(); //fire ad, reset counters
                } else
                {
                    Debug.Log("FZ:Interstitial skipped on retry , is not the time for display");
                }
                break;
            default:
                break;
        }
    }

    private void fireInterstitial()
    {
        AdmobManager.instance.ShowInterstitial(); //I had some unexplainable error, so I took it out from coroutine.
        //StartCoroutine(showAdsDelayed(0.5f));
        //Reset both counters,since we already displayed an interstitial.
        showAdsAfterRetryCounter = showAdsAfterRetry;
         showAdsEveryNLevelsCounter = showAdsEveryNLevels; //Reset Counter.
        Debug.Log("FZ:Interstitial fired");
    }

    private IEnumerator showAdsDelayed(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        AdmobManager.instance.ShowInterstitial();
    }
}

//20190108: We could control if show Interstitial vs Video Interstitial if we have two codes.
//In this way we can control when to display and when not, instead of Google.

//This class handles complexity for delegates, trying to decouple several objects.
//Theory behind:
//If I have 2 objects, A and B , and A needs to know status from B, this is possible to use events in order of:
//1. Get information in A about B'Status
//2.Decouple B from A. B just emits events, A just 'hangs' from B' events to get up to date.
//Ok, that is really nice...
//However what if A needs to know what's going on with B and B needs to know what is happening with A?
//Problem is, both objects depend each other and can't be decoupled, since every object needs to suscribe events for each other.
//The workaround is implementing a Delegate Manager. This object suscribes for every A and B objects and manages what to do in every case.
//So we are trying this

//Case 1: we need to show interstitials. BUT we only display it for every two levels.
//Case 2: if the player looses three times in a row, shows an interstitial.

//After inspection InGameUI is responsible of firing correspondent states...or I think so.
//Question: How to make sure this object is initialized AFTER the others two objects are instantiated?
//One posible solution:This objects start disabled, and then is enabled in runtime.
//I don't think is a clean solution however.

//Second question: I need some objects that should be instantiated.
//However , what object should be using singleton, what shouldn't?
//Well let's try answering this.
//What are the objects I need alive for making this happen?
//This object, DelegateManager
//At the same time, the LevelManager and InGameUI which I could get (if I instantiate after both objects)
//AdmobManager also needs to be in place. So this is the third object.
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public enum Commands
{
  noState,requestBanner,destroyBanner,requestInterstitial,showInterstitial,requestVideo,showVideo
}
//This example is based on 
//https://github.com/googleads/googleads-mobile-unity/blob/master/samples/HelloWorld/Assets/Scripts/GoogleMobileAdsDemoScript.cs
//HOWEVER, it has some absurd requirements that I'm getting rid of it which are:
//1-We don't ever need a button for destroying a Banner or Interstitial: every time we call request Banner or request Interstitial, they are auto destroyed
//I would understand if we need to do this internally, but why a button for this? Nonsense, at least NO for this example.
//2-Observe we DON'T need to request a video (the AdmobClass use a singleton to instantiate video and then never need a request.
//WHY in the earth I would need requesting banner/interstitial and THEN displaying? Again, doesn't have sense at all.

public class AdmobCallerTest : MonoBehaviour {
    private float deltaTime = 0.0f;

    private Commands cmd = Commands.noState; //We start the class with noState at all.
    public static event System.Action<Commands> CommandList = delegate { };

    public Commands callerCommand
    {
        get
        {
            return cmd;
        }
        private set
        {
            if (value != cmd)
            {
                //Debug.Log("ZZZ: GameState: changing status from " + cmd.ToString() + " to " + value.ToString());
                cmd = value;
                //Debug.Log("State changed to:" + cmd.ToString());
                CommandList(cmd);
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("AdmobCallerTest started");
    }

    public void Update()
    {
        // Calculate simple moving average for time to render screen. 0.1 factor used as smoothing
        // value.
        this.deltaTime += (Time.deltaTime - this.deltaTime) * 0.1f;
    }

    private static string outputMessage = string.Empty;

    public static string OutputMessage
    {
        set { outputMessage = value; }
    }

    public void OnGUI()
    {
        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, Screen.width, Screen.height);
        style.alignment = TextAnchor.LowerRight;
        style.fontSize = (int)(Screen.height * 0.06);
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        float fps = 1.0f / this.deltaTime;
        string text = string.Format("{0:0.} fps", fps);
        GUI.Label(rect, text, style);

        // Puts some basic buttons onto the screen.
        GUI.skin.button.fontSize = (int)(0.035f * Screen.width);
        float buttonWidth = 0.35f * Screen.width;
        float buttonHeight = 0.15f * Screen.height;
        float columnOnePosition = 0.1f * Screen.width;
        float columnTwoPosition = 0.55f * Screen.width;

        Rect requestBannerRect = new Rect(
            columnOnePosition,
            0.05f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(requestBannerRect, "Request\nBanner"))
        {
            callerCommand = Commands.requestBanner;
           // this.RequestBanner();
        }

        
        Rect destroyBannerRect = new Rect(
            columnOnePosition,
            0.225f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(destroyBannerRect, "Destroy\nBanner (deprecated)"))
        {
            Debug.Log("NOSENSE: banner destroy itself everyt time start. This button hasn't sense at all!!!!");
            callerCommand = Commands.destroyBanner;
            //this.bannerView.Destroy();
        }
        

        Rect requestInterstitialRect = new Rect(
            columnOnePosition,
            0.4f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(requestInterstitialRect, "Request\nInterstitial"))
        {
            callerCommand = Commands.requestInterstitial;
            //this.RequestInterstitial();
        }

        Rect showInterstitialRect = new Rect(
            columnOnePosition,
            0.575f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(showInterstitialRect, "Show\nInterstitial"))
        {
            callerCommand = Commands.showInterstitial;
            //this.ShowInterstitial();
        }
        /*
        Rect destroyInterstitialRect = new Rect(
            columnOnePosition,
            0.75f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(destroyInterstitialRect, "Destroy\nInterstitial"))
        {
            callerCommand = Commands.destroyInterstitial;
            //this.interstitial.Destroy();
        }
        */

        Rect requestRewardedRect = new Rect(
            columnTwoPosition,
            0.05f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(requestRewardedRect, "Request\nRewarded Video"))
        {
            callerCommand = Commands.requestVideo;
            //this.RequestRewardBasedVideo();
        }

        Rect showRewardedRect = new Rect(
            columnTwoPosition,
            0.225f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(showRewardedRect, "Show\nRewarded Video"))
        {
            callerCommand = Commands.showVideo;
            //this.ShowRewardBasedVideo();
        }

        Rect textOutputRect = new Rect(
            columnTwoPosition,
            0.925f * Screen.height,
            buttonWidth,
            0.05f * Screen.height);
        GUI.Label(textOutputRect, outputMessage);
    }

}

﻿using System;
using UnityEngine;
using GoogleMobileAds.Api;

//Documentation at class bottom
public enum AdmobStates
{
    noState, interClosed, videoRewarded, videoClosed
}

public class AdmobManager  : MonoBehaviour
{
    public static AdmobManager instance;
    private AdmobStates admobState = AdmobStates.noState; //We start the class with noState at all.
    public Boolean EnableToastForSeriousErrors = true;    //If true, serious errors are displayed on screen, 
                                                          //ONLY FOR TESTING PURPOSES.
    public static event System.Action<AdmobStates> AdmobStateChanged = delegate { };

    public AdmobStates adState
    {
        get
        {
            return admobState;
        }
        private set
        {
            //TEST:Allow repetition
            admobState = value;
            AdmobStateChanged(admobState);
            if (value != admobState)
            {

            }
            else
            {
                Msg("Same state repeated: no action taken");
            }
        }
    }


    /*  Sample ad units:  https://developers.google.com/admob/android/test-ads    */

    public const String UNUSEDADID = "unused";
    public const String UNEXPECTED = "unexpected_platform";
    public const String TEST_AD_B = "ca-app-pub-3940256099942544/6300978111";
    public const String TEST_AD_I = "ca-app-pub-3940256099942544/1033173712";
    public const String TEST_AD_IV = "ca-app-pub-3940256099942544/8691691433";
    public const String TEST_AD_RV = "ca-app-pub-3940256099942544/5224354917";
    public Boolean TestMode = false; //if True, the Class would use test ads defined above.

    public String AppId = "undefined"; //To be defined
    public String BannerCode = "";
    public String InterstitialCode = "";
    public String InterstitialVideoCode = "";
    public String VideoRewardCode = "";


    private BannerView bannerView;
    private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardBasedVideo;
    private float deltaTime = 0.0f;
    private static string outputMessage = string.Empty;

    private ShowToast ST;

    public static string OutputMessage
    {
        set { outputMessage = value; }
    }

    public void Start()
    {
        if (!instance)
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;
        }
        else
        {
            DestroyImmediate(this.gameObject);

        }

        ST = new ShowToast();
        string appId = AppId;
        MobileAds.Initialize(appId);                         // Initialize the Google Mobile Ads SDK.

        //When starting ask for an interstitial. Next time it will be required when it is closed.
        this.RequestInterstitialNormal(); //If we don't care about Interstitial /Video interstitial, we will use just one code and call always RequestInterstitialNormal()
                                          //We want to control if we are going to use Interstitial and Video Interstitial when & where, we can use RequestInterstitiaNormal y RequiestInterstitialVideo.

        this.rewardBasedVideo = RewardBasedVideoAd.Instance; // Get singleton Rewarded Video instance
                                                             // RewardBasedVideoAd is a singleton,handlers are registered once.
                                                             // Using anonymous function to minimize code
                                                             // FZSM: Please note there is NOT initialization for banner nor Interstitial, since it is simpler.
        this.rewardBasedVideo.OnAdRewarded += (object sender, Reward args) =>
        {
            adState = AdmobStates.videoRewarded;
            string type = args.Type;
            double amount = args.Amount;
            Msg("OnAdRewarded video fired for " + amount.ToString() + " " + type);
        };
 
         this.rewardBasedVideo.OnAdClosed += (object sender, EventArgs args) => {
            adState = AdmobStates.videoClosed;
            Msg("OnAdClosed video fired");
         };

        this.rewardBasedVideo.OnAdFailedToLoad += (object sender, AdFailedToLoadEventArgs args) => 
                                                  { Msg("OnAdFailedToLoad fired with message: " + args.Message,true); };

        this.rewardBasedVideo.OnAdLoaded  += (object sender, EventArgs args) => { Msg("OnAdLoaded video fired"); }; 
        this.rewardBasedVideo.OnAdOpening += (object sender, EventArgs args) => { Msg("OnAdOpening video fired"); };
        this.rewardBasedVideo.OnAdStarted += (object sender, EventArgs args) => { Msg("OnAdStarted video fired"); };
        this.rewardBasedVideo.OnAdLeavingApplication += (object sender, EventArgs args) => { Msg("OnAdLeavingApplication video fired"); };
    }

    //NOTE: Banner and Interstitials need to recreate event handler every time the object is recreated.
    //Video Reward is slightly different because is using a singleton, therefore events does not need to be regenerated
    //This is a THEORY and it needs to be checked and RE-confirmed...
    //FZSM: By default, the class is configured to server test ads.
    public void RequestBanner(AdPosition pos = AdPosition.Bottom)
    {
        string adUnitId = "";

#if UNITY_EDITOR
        adUnitId = UNUSEDADID;
#elif UNITY_ANDROID
        adUnitId = TEST_AD_B; //Default adUnit is Test Mode initialization
#else
        adUnitId = UNEXPECTED;
#endif
        if (!TestMode) { adUnitId = BannerCode; }

        // Clean up banner ad before creating a new one.
        if (this.bannerView != null) { this.bannerView.Destroy(); }

        // Create a 320x50 banner at the pos specified as parameter (default is bottom)
        this.bannerView = new BannerView(adUnitId, AdSize.SmartBanner, pos);

        // Register for ad events. FZSM: using anonynmous functions to minimize code. 
        // These events are rarely used, if at all. In case of minimizing code, let's ride it.
        this.bannerView.OnAdLoaded += (object sender, EventArgs args) => 
            {Msg("OnAdLoaded fired"); };

        this.bannerView.OnAdFailedToLoad += (object sender, AdFailedToLoadEventArgs args) => 
            { Msg("OnAdFailedToLoad fired with message: " + args.Message,true); };

        this.bannerView.OnAdOpening += (object sender, EventArgs args) => 
            { Msg("OnAdOpening fired"); };

        this.bannerView.OnAdClosed += (object sender, EventArgs args) => 
            { Msg("OnAdClosed fired"); };

        this.bannerView.OnAdLeavingApplication += (object sender, EventArgs args) => 
        { Msg("OnAdLeavingApplication fired"); };
 
        // Load a banner ad.
        this.bannerView.LoadAd(this.CreateAdRequest());

        if (TestMode)
        {
            Msg("Loading banner ad with code:" + adUnitId + " (test banner)" );
        } else
        {
            Msg("Loading banner ad with code :" + adUnitId);
        }
        adState = AdmobStates.noState; //Initialize State
    }

    public void DestroyBanner()
    {
        this.bannerView.Destroy();
    }


    //Overloaded options: allows control interstitial using common interstitial or video-based.
    //In order to do that: we need TWO intertstial codes: uno with only text/images and other only video based. 
    //(You need to create it on Admob web page)
    //Ok I moved the definition and declaration here, however how can I change what code I should use, since it uses
    //only Interstitial code? 
    //Short Answer: Delegate Manager has the power of doing this, calling RequestInterstitial (code)
    //The logic for alternation is OUTSIDE AdmobManager. Delegate Manager needs to sort this out, that's why decoupling was made

    public void RequestInterstitialNormal()
    {
        string adUnitId;
        #if UNITY_EDITOR
            adUnitId = UNUSEDADID;
        #elif UNITY_ANDROID
            adUnitId = TEST_AD_I;
        #else
            adUnitId =UNEXPECTED;
        #endif
        if (!TestMode)
        {
            adUnitId = InterstitialCode;
        }
        RequestInterstitial(adUnitId);
    }

    public void RequestInterstitialVideo()
    {
        string adUnitId;
        #if UNITY_EDITOR
            adUnitId = UNUSEDADID;
        #elif UNITY_ANDROID
            adUnitId = TEST_AD_IV;
        #else
            adUnitId =UNEXPECTED;
        #endif  
        if (!TestMode)
        {
            adUnitId = InterstitialVideoCode;
        }
        RequestInterstitial(adUnitId);
    }

    //FZSM: by default class is configured to serve test ads.
    private void RequestInterstitial(string adCode)
    {

        if (adCode.Trim().Length==0)
        {
            Msg("ERROR:Interstitial Ad code empty....",true);
        }

        // Clean up interstitial ad before creating a new one.
        if (this.interstitial != null)  { this.interstitial.Destroy(); }

        // Create an interstitial.
        this.interstitial = new InterstitialAd(adCode);
        // Register for ad events. Due many of the events are unusued, I added anonymous functions with some feedback shortening the code.
        this.interstitial.OnAdLoaded += (object sender, EventArgs args) => { Msg("OnAdLoaded Interstitial fired"); };
        this.interstitial.OnAdFailedToLoad += (object sender, AdFailedToLoadEventArgs args) => 
                                              {Msg( "OnAdFailedToLoad Interstitial fired with message: " + args.Message,true);};
        this.interstitial.OnAdOpening += (object sender, EventArgs args) => { Msg("OnAdOpening Interstitial fired"); };
        this.interstitial.OnAdLeavingApplication += (object sender, EventArgs args) =>  
                                                    { Msg("OnAdLeavingApplication Interstitial fired"); };
        this.interstitial.OnAdClosed += (object sender, EventArgs args) =>
            {
                adState = AdmobStates.interClosed;
                Msg("OnAdClosed Interstitial event received");
            };
        
        this.interstitial.LoadAd(this.CreateAdRequest());// Load an interstitial ad.
        if (TestMode)
        {
            Msg("Loading interstitial ad with code:" + adCode + " (test Interstitial)");
        }
        else
        {
            Msg("Loading interstitial ad with code:" + adCode);
        }

        adState = AdmobStates.noState; //Initialize State
    }

    public void RequestRewardBasedVideo()
    {
#if UNITY_EDITOR
        string adUnitId = UNUSEDADID;
#elif UNITY_ANDROID
        string adUnitId = TEST_AD_RV;
#else
        string adUnitId =UNEXPECTED;
#endif

        if (!TestMode)
        {
            adUnitId = VideoRewardCode;
            if (adUnitId.Trim().Length == 0)
            {
                Msg("ERROR:Interstitial Ad code empty....",true);
            }
        }

        if (TestMode)
        {
            Msg("Loading video ad with code:" + adUnitId + " (test video)");
        }
        else
        {
            Msg("Loading video ad with code:" + adUnitId);
        }

        this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), adUnitId);
        adState = AdmobStates.noState; //Initialize State
    }


    public void ShowInterstitial()
    {
        try
        {
            if (this.interstitial == null)
            {
                Msg("Interstitial null, did you requested first?", true);
                return;
            }
            if (!this.interstitial.IsLoaded())
            {
                Msg("Interstitial is not ready yet", true);
                return;
            }
            this.interstitial.Show();
        }
        catch (Exception ex)
        {
            Msg("Some error on ShowInterstitial:" + ex.Message);
            return;
        }
    }

    public void ShowRewardBasedVideo()
    {
        if (!this.rewardBasedVideo.IsLoaded())
        {
            Msg("Reward based video ad is not ready yet",true);
            return;
        }
        this.rewardBasedVideo.Show();
    }

    private AdRequest CreateAdRequest()
    {
        //FZSM: Android emulators are automatically configured as test devices. (https://developers.google.com/admob/android/test-ads)
        return new AdRequest.Builder().Build();
    }

    private void Msg (String msg)
    {
        Msg(msg, false);
    }

    private void Msg (String msg,Boolean ShowToast)
    {
        MonoBehaviour.print(msg);
        if (ShowToast && EnableToastForSeriousErrors)
        {
            ST.Msg(msg);
        }
    }
}

//This is an state-of-the-art-admob-class and it has to help me back as much as I did for this class.
// AdMob Invoking
//20190101: VERY IMPORTANT:
//This new Ad class involves few new concepts:
//-A: Class fires states to a DelegateManager Object.
//-B: Class is controlled with DelegateManager Object. Therefore, NO other class would refer to this class in the entire project
//-C: In the same fashion, this class NEVER refers to another class. 
//A,B,C allows to make something new and IMPORTANT:
//-This class is COMPLETELY PORTABLE BETWEEN PROJECTS. All the burden is handled by DelegateManager (from now DM), who has the intelligence/complexity for deciding when to call this class.
//-DM is the 'connection' between Admob and ANY project, so change DM accordingly every project.

//Some few wierd things:
//1-Request banner automatically displays and ad
//2-Request Interstitial gets the Interstitial but you will need to Display it later.
//3-Same as video, however video uses Singleton, so the instance doesn't have to be recreated.

//Current versions and history:
//1- Simplification: concentrate in ONE focus: Android, get rid of IOS
//2: Using delegates
//Using delegates we can decouple objects. However, to effectively use delegates, we need using a DelegateManager class (DM) handling //
//all code needed complexity. The DM class is reponsible of binding class to any software.

//If we don't use it, we half-decouple a solution. If we have two objects A and B, if A depends on B we can use delegates and that's it.
//However if A depends on B y B depends on A, we need add delegates in both objects, but we can't decouple it.

    //3:20190103: I can prove my theory works and is ok. We can use delegates to decouple object completely in most cases.
//- Making simplification using anonymous/lambda functions.
// https://social.msdn.microsoft.com/Forums/vstudio/en-US/90694d50-92dd-4f23-af22-8ec2db6693b9/lambda-expression-in-event-handler-assignment?forum=csharpgeneral
//So the following example:
//        this.rewardBasedVideo.OnAdLoaded += this.HandleRewardBasedVideoLoaded;
//            ...
//    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
//        {
//            MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
//        }
//Can be replaced with:
//        this.rewardBasedVideo.OnAdLoaded += (object sender, EventArgs args) => { MonoBehaviour.print("HandleRewardBasedVideoLoaded event received"); };
//4:20190104: Trying to make a complete test using a onGui example
//5: Success on splitting code. I got some bugs in the first tests
//6: Added a Toast Class to display messages as helper class.
//7- Class shrinked about 150 lines. Howevr, since I got rid of OnGui method, the real shrink is only 50 lines...
//8-I've tested many times and for some strange reason I got Error 3 no fill on video rewards. That should be impossible,
//since, THIS IS A TEST not a regular add. However, I'm having this unexplainable error always.
//Important detail: if a Video Reward return an error, Interstitial ad can't be displayed unless we re-request an interstitial ad.
//I don't know the relationship but I cant test this behavior on the test scene.


    /*
       private AdRequest CreateAdRequest()
    {
        //FZSM: Remember:
        //Android emulators are automatically configured as test devices. (https://developers.google.com/admob/android/test-ads)

        return new AdRequest.Builder().Build();
        /*
        .AddTestDevice(AdRequest.TestDeviceSimulator)
        .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
        .AddKeyword("game")
        .SetGender(Gender.Male)
        .SetBirthday(new DateTime(1985, 1, 1))
        .TagForChildDirectedTreatment(false)
        .AddExtra("color_bg", "9B30FF")

    }
*/
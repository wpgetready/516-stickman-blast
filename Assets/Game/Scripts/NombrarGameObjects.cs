﻿using UnityEngine;

public class NombrarGameObjects : MonoBehaviour {

    public bool decimales = true;

    char[] nameLvlChar;
    private string newLvlName = "";

    void Start () {

        if(decimales)
        {
            for (int i = 0; i < 10; i++)
            {
                nameLvlChar = transform.GetChild(i).gameObject.name.ToCharArray();
                newLvlName = 
                    nameLvlChar[0].ToString() +
                    nameLvlChar[1].ToString() + 
                    nameLvlChar[2].ToString() + 
                    nameLvlChar[3].ToString() + 
                    nameLvlChar[4].ToString() + "0" + 
                    nameLvlChar[5].ToString() +
                    nameLvlChar[6].ToString();

                transform.GetChild(i).gameObject.name = newLvlName;
            }
        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                
                if(i == 9)
                {
                    nameLvlChar = transform.GetChild(i).gameObject.name.ToCharArray();
                    newLvlName = nameLvlChar[0].ToString() + nameLvlChar[1].ToString() + nameLvlChar[2].ToString() + nameLvlChar[3].ToString() + nameLvlChar[4].ToString() + "0" + nameLvlChar[5].ToString() + nameLvlChar[6].ToString();
                    transform.GetChild(i).gameObject.name = newLvlName;
                }
                else
                {
                    nameLvlChar = transform.GetChild(i).gameObject.name.ToCharArray();
                    newLvlName = nameLvlChar[0].ToString() + nameLvlChar[1].ToString() + nameLvlChar[2].ToString() + nameLvlChar[3].ToString() + nameLvlChar[4].ToString() + "00" + nameLvlChar[5].ToString();
                    transform.GetChild(i).gameObject.name = newLvlName;
                }
            }
        }
    }
}

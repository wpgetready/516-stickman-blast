﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Linescript : MonoBehaviour {

	public Transform ConnectedObj;
    [SerializeField] private Vector2 offSet = Vector2.zero;

	LineRenderer linerend;

	void Start () {
		linerend = GetComponent<LineRenderer>();
	}
	
	void Update () {
        if(ConnectedObj != null)
        {
            linerend.SetPosition(0, transform.position + new Vector3(offSet.x, offSet.y, 0f));
            linerend.SetPosition(1, ConnectedObj.position);
        }
    }

    public void ConnectedTransform(Transform obj)
    {
        ConnectedObj = obj;
    }
}

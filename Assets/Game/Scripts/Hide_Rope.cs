﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide_Rope : MonoBehaviour {

	public string objName;
	public GameObject particles;
	SoundController mySound;

    //20190109: Found where this thing is coming from!
    // https://docs.unity3d.com/ScriptReference/RaycastHit2D-collider.html
    //In short, ANY OBJECT NAMED 'red' is capable of dissapearing.
    //20190109: Improved due an issue , more specific in level 021 Stickman Draft - Blast
    //Explained and discussed in the tasklist

    void Start(){
		mySound = FindObjectOfType<SoundController> ();
	}

	void Update(){
		
		if (Input.GetMouseButtonDown (0)) {

			Vector2 worldPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);
            RaycastHit2D[] hits=   Physics2D.RaycastAll(worldPoint, Vector2.zero);
            if (hits.Length == 0) return;
            int counter = 0;

            foreach (RaycastHit2D hit in hits)
            {
                
                if (hit.collider.name == objName)
                {
                   // Debug.Log("We have a match!:" + hit.collider.name + " counter:" + counter.ToString());
                    hit.collider.gameObject.SetActive(false);
                    Instantiate(particles, hit.transform.position, Quaternion.identity);
                    if (mySound != null)
                    {
                        mySound.tap.Play();
                        break; //Only one collider
                    }
                } else
                {
//                    Debug.Log("this object doesn't work:" + hit.collider.name + "counter:" + counter.ToString());
                }
                counter++;

            }
        }
	}
}
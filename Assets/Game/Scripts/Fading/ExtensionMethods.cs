﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//20190106: This clearly be not understandable. This class corresponds to the Extension Methods
//Source: https://unity3d.com/learn/tutorials/topics/scripting/extension-methods
//with this, we can extend ANYTHING we want, for example a float value.
//Why we would do this? Because we can solve complex things easily. For example, fading an image or object without effort and using one line.
//See the examples
//https://stackoverflow.com/questions/37223919/create-a-coroutine-to-fade-out-different-types-of-object


public static class ExtensionMethods
{
    public static IEnumerator Tweeng(this float duration,
               System.Action<float> var, float aa, float zz)
    {
        float sT = Time.time;
        float eT = sT + duration;

        while (Time.time < eT)
        {
            float t = (Time.time - sT) / duration;
            var(Mathf.SmoothStep(aa, zz, t));
            yield return null;
        }

        var(zz);
    }

    public static IEnumerator Tweeng(this float duration,
               System.Action<Vector3> var, Vector3 aa, Vector3 zz)
    {
        float sT = Time.time;
        float eT = sT + duration;

        while (Time.time < eT)
        {
            float t = (Time.time - sT) / duration;
            var(Vector3.Lerp(aa, zz, t));
            yield return null;
        }

        var(zz);
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

public class FadeObject : MonoBehaviour {
    void Start() {
        Image img =  gameObject.GetComponent<Image>();
        StartCoroutine(5f.Tweeng((u) => { Color tmp = img.color; tmp.a = u; img.color = tmp; }, 0f, 1f));
    }
}
